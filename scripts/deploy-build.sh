#!/bin/bash

# Redirect STDOUT to STDOUT (outside script)
exec 1>&1
# Redirect STDERR to STDERR (outside script)
exec 2>&2

CWD=$(dirname $(realpath $0))

APPDIR=$(realpath $CWD/../../app)
GITDIR=$(realpath $CWD/../../app.git)

echo "APP DIR: $APPDIR"
echo "GIT DIR: $GITDIR"
echo "Moving to work tree directory: $APPDIR"

cd $APPDIR

# Echoes all commands before executing.
set -o verbose

# Maintenance tasks
yarn install --save --verbose
yarn build

# CLEAN OPCACHE
# apt-get install libfcgi0ldbl
# wget https://gist.githubusercontent.com/muhqu/91497df3a110f594b992/raw/58bf4ee037b637c24dea2f80a8c6735d5229f4d6/php-fpm-cli
# chmod +x php-fpm-cli
# mv php-fpm-cli /usr/local/bin
php-fpm-cli -connect localhost:9013 -r "opcache_reset();"
