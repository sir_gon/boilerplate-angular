#!/bin/bash

# Redirect STDOUT to STDOUT (outside script)
exec 1>&1
# Redirect STDERR to STDERR (outside script)
exec 2>&2

ssh gon@gon.cl 'echo "Iniciando DEPLOY"; \
	bash /home/gon/web/memoria-anual-2016.gon.cl/app/scripts/deploy-files.sh; \
	bash /home/gon/web/memoria-anual-2016.gon.cl/app/scripts/deploy-build.sh; \
	'
