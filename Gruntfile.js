/* globals module */
module.exports = function(grunt) {
	'use strict';

	//Load grunt modules tasks
	require('load-grunt-tasks')(grunt);
	require('time-grunt')(grunt);

	var AppConfig = {
		version: require('./package.json').version,
		base: 'app',
		dist: 'dist'
	};

	grunt.initConfig({
		config: AppConfig,
		//Watch
		// Watches the files specified and executes the tasks.
		watch: {
			php: {
				files: ['<%= config.base %>/index.php'],
				options: {
					livereload: 35729
				}
			},
			less: {
				files: ['<%= config.base %>/styles/{,**/}*.less'],
				tasks: ['newer:jsbeautifier', 'less:dev', 'autoprefixer:dev', 'cssmin:dev'],
				options: {
					livereload: 35729
				}
			},
			js: {
				files: ['<%= config.base %>/scripts/{,**/}*.js'],
				tasks: ['newer:jsbeautifier'],
				options: {
					livereload: 35729
				}
			},
			hmtl: {
				files: ['<%= config.base %>/views/{,**/}*.html'],
				tasks: ['newer:jsbeautifier'],
				options: {
					livereload: 35729
				}
			},
			img: {
				files: ['<%= config.base %>/img/{,**/}*'],
				tasks: ['newer:imagemin:src'],
				options: {
					livereload: 35729
				}
			},
			gruntfile: {
				files: ['Gruntfile.js'],
				tasks: ['jsbeautifier'],
				options: {
					reload: true
				}
			},
			packageJson: {
				files: ['package.json'],
				tasks: ['replace:dev', 'replace:dist'],
				options: {
					livereload: 35729
				}
			}
		},
		//PHP
		// Sets up PHP server running on specified port
		php: {
			dev: {
				options: {
					base: '<%= config.base %>',
					hostname: '0.0.0.0',
					port: 8000,
				}
			},
			dist: {
				options: {
					base: '<%= config.dist %>',
					hostname: '0.0.0.0',
					port: 8000,
				}
			}
		},
		//JSHint
		// Checks JS code for errors and report to user
		jshint: {
			options: {
				reporter: require('jshint-stylish'),
				jshintrc: true,
			},
			files: ['Gruntfile.js', '<%= config.base %>/scripts/{,**/}*.js']
		},
		//Clean
		// Deletes files and folders with the specified pattern
		clean: {
			prebuildfast: {
				src: [
					'<%= config.dist %>'
				]
			},
			prebuild: {
				src: [
					'<%= config.dist %>',
					'<%= config.base %>/vendor/*',
					'!<%= config.base %>/vendor/*.custom',
				]
			},
			postbuild: {
				src: ["<%= config.dist %>/styles/less/",
					"<%= config.dist %>/styles/style.css",
				]
			}
		},
		//Copy
		// Copies the files and folders specified
		copy: {
			build: {
				files: [{
					expand: true,
					dot: true,
					cwd: '<%= config.base %>',
					dest: '<%= config.dist %>',
					src: '{,**/}*'
				}]
			}
		},
		//Shell
		// Executes a command
		// Homepage: https://github.com/sindresorhus/grunt-shell
		shell: {
			bower: {
				command: 'bower update --allow-root'
			},
		},
		//JSBeautifier
		// Parse HTML, JS and CSS files, and beautifies code according specified rules
		jsbeautifier: {
			files: [
				/* No beautificar estas carpetas o archivos */
				"!app/vendor/",
				"!node_modules/",
				"!nbproject/",
				"!.git/",

				/* App */
				"app/views/{,**/}*.html",
				"app/scripts/{,**/}*.js",
				"app/styles/less/{,**/}*.less",

				/* Tests */
				"test/{,**/}*.js",

				/* Proyecto */
				"*.json",
				"*.js",
				".bowerrc",
				".jshintrc"
			],
			options: {
				html: {
					braceStyle: "end-expand",
					indentWithTabs: true,
					indentScripts: "normal",
					indentSize: 1,
					maxPreserveNewlines: 2,
					wrapLineLength: 0
				},
				js: {
					fileTypes: [".json", ".bowerrc", ".jshintrc"],
					braceStyle: "end-expand",
					breakChainedMethods: false,
					e4x: false,
					evalCode: false,
					indentSize: 1,
					indentWithTabs: true,
					jslintHappy: false,
					keepArrayIndentation: false,
					maxPreserveNewlines: 2,
					spaceBeforeConditional: true,
					spaceInParen: false,
					unescapeStrings: false,
					wrapLineLength: 0,
					endWithNewline: true
				},
				css: {
					fileTypes: [".less"],
					braceStyle: "end-expand",
					indentWithTabs: true,
					indentSize: 1,
					maxPreserveNewlines: 2,
					endWithNewline: true,
					wrapLineLength: 0
				}
			}
		},
		//LESS
		// Compiles .less files into .css
		less: {
			dev: {
				options: {
					compress: false
				},
				files: {
					'<%= config.base %>/styles/style.css': '<%= config.base %>/styles/less/app.less'
				}
			},
			dist: {
				options: {
					compress: false
				},
				files: {
					'<%= config.dist %>/styles/style.css': '<%= config.dist %>/styles/less/app.less'
				}
			}
		},
		//Autoprefixer
		// Adds modificators as "-webkit", "-moz", etc
		autoprefixer: {
			options: {
				browsers: [
					'Android >= 2.3',
					'BlackBerry >= 7',
					'Chrome >= 9',
					'Firefox >= 4',
					'Explorer >= 9',
					'iOS >= 5',
					'Opera >= 11',
					'Safari >= 5',
					'OperaMobile >= 11',
					'OperaMini >= 6',
					'ChromeAndroid >= 9',
					'FirefoxAndroid >= 4',
					'ExplorerMobile >= 9'
				]
			},
			dev: {
				expand: true,
				flatten: true,
				src: '<%= config.base %>/styles/style.css',
				dest: '<%= config.base %>/styles/'
			},
			dist: {
				expand: true,
				flatten: true,
				src: '<%= config.dist %>/styles/style.css',
				dest: '<%= config.dist %>/styles/'
			}
		},
		//CSSMin
		// Minifies CSS file to reduce space and network traffic
		cssmin: {
			dev: {
				files: {
					'<%= config.base %>/styles/style.min.css': [
						'<%= config.base %>/styles/style.css'
					]
				}
			},
			dist: {
				files: {
					'<%= config.dist %>/styles/style.min.css': [
						'<%= config.dist %>/styles/style.css'
					]
				}
			}
		},
		//HTMLMin
		// Minifies HTML files to reduce space and network traffic
		htmlmin: {
			dist: {
				options: {
					collapseWhitespace: true,
					conservativeCollapse: true,
					collapseBooleanAttributes: true,
					removeCommentsFromCDATA: true,
					removeOptionalTags: true
				},
				files: [{
					expand: true,
					cwd: '<%= config.dist %>',
					src: ['*.html', 'views/{,**/}*.html'],
					dest: '<%= config.dist %>'
				}]
			}
		},
		//ImageMin
		// Compress image files to reduce space and network traffic
		imagemin: {
			src: {
				files: [{
					expand: true,
					cwd: '<%= config.base %>/img',
					src: '{,**/}*.{png,jpg,jpeg,gif}',
					dest: '<%= config.base %>/img'
				}]
			},
			dist: {
				files: [{
					expand: true,
					cwd: '<%= config.dist %>/img',
					src: '{,**/}*.{png,jpg,jpeg,gif}',
					dest: '<%= config.dist %>/img'
				}]
			}
		},
		//ngAnnotate
		// Adds and removes angular injection annotations
		ngAnnotate: {
			remove: {
				options: {
					add: false,
					remove: true
				},
				files: [{
					expand: true,
					cwd: '<%= config.base %>/scripts/',
					src: '{,**/}*.js',
					dest: '<%= config.base %>/scripts/'
				}]
			},
			dev: {
				files: [{
					expand: true,
					cwd: '<%= config.base %>/scripts/',
					src: '{,**/}*.js',
					dest: '<%= config.base %>/scripts/'
				}]
			},
			dist: {
				files: [{
					expand: true,
					cwd: '<%= config.dist %>/scripts/',
					src: '{,**/}*.js',
					dest: '<%= config.dist %>/scripts/'
				}]
			}
		},
		//Uglify
		// Compress code and replaces variable names to reduce code size and obfuscate lecture
		uglify: {
			options: {
				mangle: {
					toplevel: true
				},
				squeeze: {
					dead_code: false
				},
				codegen: {
					quote_keys: true
				}
			},
			dist: {
				files: [{
					expand: true,
					cwd: '<%= config.dist %>/scripts',
					src: '{,**/}*.js',
					dest: '<%= config.dist %>/scripts'
				}]
			}
		},
		//Replace
		// Replaces strings across files
		replace: {
			cleandev: {
				options: {
					patterns: [{
						match: /.*console.dir\(.*\)[\s;]*[\n\r]+/ig,
						replacement: ''
					}, {
						match: /.*console.log\(.*\)[\s;]*[\n\r]+/ig,
						replacement: ''
					}, {
						match: /.*debugger[\s;]*[\n\r]+/ig,
						replacement: ''
					}]
				},
				files: [{
					expand: true,
					cwd: '<%= config.base %>/scripts/',
					src: '{,**/}*.js',
					dest: '<%= config.base %>/scripts/'
				}]
			},
			dev: {
				options: {
					patterns: [{
						match: /\$appVersion.\s*\=\s*.+/ig,
						replacement: '$appVersion = \'' + AppConfig.version + "\';"
					}]
				},
				files: {
					'<%= config.base %>/index.php': '<%= config.base %>/index.php'
				}
			},
			dist: {
				options: {
					patterns: [{
						match: /\$appVersion.\s*\=\s*.+/ig,
						replacement: '$appVersion = \'' + AppConfig.version + "\';"
					}]
				},
				files: {
					'<%= config.dist %>/index.php': '<%= config.dist %>/index.php'
				}
			}
		}
	});

	//Task: 		serve
	//Call as: 		'grunt serve' or 'grunt serve:dist'
	//Description:	sets up a web server to run app
	grunt.registerTask('serve', 'Compile then start a connect web server', function(target) {
		if (target === 'dist') {
			return grunt.task.run(['build', 'php:dist', 'watch']);
		}

		grunt.task.run(['jsbeautifier', 'less:dev', 'autoprefixer:dev', 'replace:dev', 'cssmin:dev', 'php:dev', 'watch']);
	});

	//Task: 		prepare
	//Call as: 		'grunt prepare'
	//Description:	removes annotations, sets app version into index.php, beautifies code and check for errors
	grunt.registerTask('prepare', ['ngAnnotate:remove', 'replace:cleandev', 'replace:dev', 'jsbeautifier', 'jshint', 'newer:imagemin:src']);

	//Task: 		build
	//Call as: 		'grunt build' or 'grunt build:fast'
	//Description:	compiles project into a deployable folder. If argument 'fast' argument, avoid ImageMin and Shell (bower install) [use with care, only if dependencies are recently updated]
	grunt.registerTask('build', function(target) {
		//Avoid deleting vendor and imagemin
		if (target === 'fast') {
			return grunt.task.run(['jshint', 'clean:prebuildfast', 'shell:bower', 'copy:build', 'less:dist', 'autoprefixer:dist', 'replace:dist', 'cssmin:dist', 'htmlmin:dist', 'ngAnnotate:dist', 'uglify:dist', 'clean:postbuild']);
		}
		//Forces reinstall vendor
		else if (target === 'full') {
			return grunt.task.run(['jshint', 'clean:prebuild', 'shell:bower', 'newer:imagemin:src', 'copy:build', 'less:dist', 'autoprefixer:dist', 'replace:dist', 'cssmin:dist', 'htmlmin:dist', 'ngAnnotate:dist', 'uglify:dist', 'clean:postbuild']);
		}
		else {
			return grunt.task.run(['jshint', 'clean:prebuildfast', 'shell:bower', 'copy:build', 'less:dist', 'autoprefixer:dist', 'replace:dist', 'cssmin:dist', 'htmlmin:dist', 'ngAnnotate:dist', 'uglify:dist', 'clean:postbuild']);
		}
	});

	//Task: 		default
	//Call as: 		'grunt' or 'grunt default'
	//Description:	by default, calls 'grunt serve'
	grunt.registerTask('default', 'Compile then start a connect web server', function() {
		grunt.task.run('serve');
	});
};
