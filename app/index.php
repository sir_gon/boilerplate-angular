<?php
$env = getenv("ENVIRONMENT_TYPE");
$tmKey = "XXX-XXXXXX";
$appVersion = '0.0.1';
$useDevLibraries = false;

$JSVARS = [];

if ($env != "PROD") {
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
}

$JSVARS["PRODUCT_NAME"] = "sonda-memoria-2016";
$JSVARS["APP_VERSION"] = $appVersion;

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<!-- STATIC CODE -->
	<meta charset="utf-8" />
	<base href="<?= getenv('BASE_HREF') ? getenv('BASE_HREF') : '/' ?>">

	<script src="vendor/jquery/dist/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="styles/style.min.css?v=<?= $appVersion ?>">

	<meta name="viewport" content="width=device-width,initial-scale=1">

	<!-- END OF STATIC CODE -->
</head>
<body ng-app layout="row">

	<!-- Setting global JS variables -->
	<script>
		<?php
			foreach ($JSVARS as $key => $value) {
				if (is_string($value)) {
					echo "window.$key='$value'; ";
				}
				else {
					echo "window.$key=$value; ";
				}
			}
			//echo $consoleRedefinition;
		?>
	</script>
	<!-- End of setting global JS variables -->

	<div layout="column" flex>

		<!-- <div ui-view="header"></div> -->

		<md-content class="main-body-container" flex>
			<div ui-view="body" class="main-body"></div>
		</md-content>

		<!--
		<div layout="row" class="footer" layout-align="center center">
			<div ui-view="footer"></div>
		</div>
		-->

	</div>

	<!-- Modals -->

	<script src="vendor/requirejs/require.js?v=<?= $appVersion ?>" data-main="scripts/main.js?v=<?= $appVersion ?>"></script>

	<?php
		if (in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1','::1'))) {
			$livereloadPort = getenv('LIVERELOAD') ? getenv('LIVERELOAD') : '35729';
			echo "<script src='http://localhost:" . $livereloadPort ."/livereload.js'></script>";
		}
	?>

</body>
</html>
