define(['angular'], function(angular) {
	'use strict';

	angular.module('SondaAPP.controllers.PageNotFoundController', [])
		.controller('PageNotFoundController', function($scope) {
			$scope.content = {};

			$scope.view = {};

		});
});
