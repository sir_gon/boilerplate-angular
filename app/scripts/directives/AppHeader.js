define(['angular'], function(angular) {
	'use strict';

	angular.module('SondaAPP.directives.AppHeader', [])
		.directive('appHeader', function($state) {
			return {
				restrict: 'E',
				/*
				scope: {
					showButtons: '=',
				},
				*/
				templateUrl: 'views/directives/app-header.html',
				link: function($scope, element, attrs) {

				}
			};
		});
});
