define(['angular'], function(angular) {
	'use strict';

	angular.module('SondaAPP.directives.AppFooter', [])
		.directive('appFooter', function($state) {
			return {
				restrict: 'E',
				/*
				scope: {
					showButtons: '=',
				},
				*/
				templateUrl: 'views/directives/app-footer.html',
				link: function($scope, element, attrs) {

					$scope.view.showButtons = true;

					if ($state.current.name === 'home') {
						$scope.view.showButtons = false;
					}

					$scope.view.goItinerary = function() {
						$state.go('itinerary', {
							bnumber: $state.params.bnumber,
							pass: $state.params.pass
						});

					};

					$scope.view.goPassengers = function() {
						$state.go('passengers', {
							bnumber: $state.params.bnumber,
							pass: $state.params.pass
						});
					};

					$scope.view.goAnothers = function() {
						$state.go('another', {
							bnumber: $state.params.bnumber,
							pass: $state.params.pass
						});

					};

				}

			};
		});
});
