/*jshint unused: vars */
require.config({
	waitSeconds: 200,
	urlArgs: 'v=' + window.APP_VERSION,
	paths: {
		//Bower dependencies
		'lodash': '../vendor/lodash/dist/lodash.min',
		'angular': '../vendor/angular/angular.min',
		'angular-i18n': '../vendor/angular-i18n/angular-locale_es-cl',
		'angular-cookies': '../vendor/angular-cookies/angular-cookies.min',
		'angular-sanitize': '../vendor/angular-sanitize/angular-sanitize.min',
		'angular-ui-bootstrap': '../vendor/angular-bootstrap/ui-bootstrap-tpls.min',
		'angular-ui-route': '../vendor/angular-ui-router/release/angular-ui-router.min',
		'bootstrap': '../vendor/bootstrap/dist/js/bootstrap.min',
		'moment': '../vendor/moment/min/moment.min',
		'moment-locale': '../vendor/moment/locale/es',
		'angular-aria': '../vendor/angular-aria/angular-aria.min',
		'angular-moment': '../vendor/angular-moment/angular-moment.min',
	},
	shim: {
		'angular': {
			'deps': ['lodash'],
			'exports': 'angular'
		},
		'angular-i18n': ['angular'],
		'angular-aria': ['angular'],
		'angular-sanitize': ['angular'],
		'angular-ui-route': ['angular'],
		'angular-ui-bootstrap': ['angular', 'bootstrap'],
		'bootstrap': ['angular'],
		'moment-locale': ['moment'],
		'angular-moment': ['angular', 'moment'],

		'app': ['angular']
	},
	priority: [
		'angular',
		'moment'
	]
});

//http://code.angularjs.org/1.2.1/docs/guide/bootstrap#overview_deferred-bootstrap
window.name = 'NG_DEFER_BOOTSTRAP!';

require([
	'angular',
	'app',
	'angular-i18n',
	'angular-moment',
	'angular-sanitize',
	'angular-ui-route',
	'angular-ui-bootstrap',
	'bootstrap',
	'lodash',
	'moment',
	'moment-locale',

], function(angular, app) {
	'use strict';

	angular.element().ready(function() {
		angular.resumeBootstrap([app.name]);
	});
});
