define([
		'angular',

		//App's
		'controllers/HomeController',
		'controllers/utils/PageNotFoundController',
		'directives/AppFooter',
		'directives/AppHeader'

	] /*deps*/ ,
	function(angular)
	/*invoke*/
	{
		'use strict';
		return angular
			.module('SondaAPP', [
				//Vendor modules
				'ui.router',
				'ngSanitize',
				'ui.bootstrap',
				//App
				'SondaAPP.controllers.HomeController',
				'SondaAPP.controllers.PageNotFoundController',
				'SondaAPP.directives.AppFooter',
				'SondaAPP.directives.AppHeader'

			])
			.config(function($windowProvider, $locationProvider, $stateProvider, $compileProvider, $urlRouterProvider) {
				$locationProvider.html5Mode(true); // use the HTML5 History API
				$compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel):/);

				//DOCS: https://github.com/angular-ui/ui-router/wiki
				$stateProvider
					.state('home', {
						url: '/',
						views: {
							'header': {
								templateUrl: 'views/partials/main-header.html',
								controller: 'HeaderController'
							},
							'body': {
								templateUrl: 'views/pages/home.html',
								controller: 'HomeController'
							},
							'footer': {
								templateUrl: 'views/partials/main-footer.html',
								controller: 'FooterController'
							}
						}
					});

				$urlRouterProvider.otherwise('/no-encontrado');

			})
			.run(function($rootScope, $state) {
				console.info('[App]', 'App running');

				$rootScope.$on('$stateChangeStart', function(evt, to, params) {
					if (to.redirectTo) {
						evt.preventDefault();
						$state.go(to.redirectTo, params);
					}
				});

			});
	});
